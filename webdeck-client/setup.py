from setuptools import find_packages, setup

with open("README.md", "r") as fp:
    long_description = fp.read()

setup(
    name="webdeck-client",
    description="API client for WebDeck, a web-based API and GUI for Elgato Stream Deck",
    long_description=long_description,
    long_description_content_type="text/markdown",
    use_scm_version={"root": ".."},
    setup_requires=["setuptools_scm"],
    author="Matthias Lohr",
    author_email="mail@mlohr.com",
    url="https://gitlab.com/MatthiasLohr/webdeck",
    license="MIT",
    install_requires=[],
    python_requires=">=3.8.*, <4",
    packages=find_packages(include=["webdeck_client.*"]),
    package_data={
        "webdeck_client": ["py.typed"],
    },
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Environment :: Console",
        "Environment :: Web Environment",
        "Intended Audience :: Developers",
        "Intended Audience :: End Users/Desktop",
        "Intended Audience :: System Administrators",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Typing :: Typed",
    ],
    project_urls={
        # 'Documentation': 'https://matthiaslohr.gitlab.io/webdeck-server/',
        "Source": "https://gitlab.com/MatthiasLohr/webdeck",
        "Tracker": "https://gitlab.com/MatthiasLohr/webdeck/issues",
    },
)
