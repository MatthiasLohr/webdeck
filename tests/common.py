from __future__ import annotations

from typing import Any
from unittest import TestCase

from StreamDeck.DeviceManager import DeviceManager, ProbeError


class DeviceRequiredTestCase(TestCase):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self._device_manager: DeviceManager | None = None

    def setUp(self) -> None:
        try:
            self._device_manager = DeviceManager()
        except ProbeError as e:
            self.skipTest(str(e))
            return

        if len(self._device_manager.enumerate()) == 0:
            self.skipTest("no device connected")

    def get_device_manager(self) -> DeviceManager:
        self.assertIsNotNone(self._device_manager)
        return self._device_manager
