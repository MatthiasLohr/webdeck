# Contribution Guidelines

## Set Up Development Environment

  * Fork this repository to your personal namespace.
  * Clone from your forked repository.
  * `cd` into the forked directory and create and load a virtual environment:
    ```
    virtualenv -p python3.9 venv
    . ./venv/bin/activate
    ```
  * Install the webdeck packages in editable mode to your virtual environment:
    ```
    pip install -e ./webdeck ./webdeck-client
    ```
  * Install the [development requirements](requirements-dev.txt) to your virtual environment:
    ```
    pip install -r requirements-dev.txt
    ```
  * Set up pre-commit hooks:
    ```
    pre-commit install
    ```
