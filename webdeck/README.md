# WebDeck API Server and GUI

## Requirements

To be able to communicate with the Elgato Stream Deck, `libhidapi-libusb0` has to be installed
(read https://python-elgato-streamdeck.readthedocs.io/ for more information).
