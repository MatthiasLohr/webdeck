# WebDeck

*Web-based API and GUI for Elgato Stream Deck*

This repository contains two packages:

The [webdeck](./webdeck) packages provides a web-based RPC and a GUI for working with your Elgato Stream Deck.

The [webdeck-client](./webdeck-client) packages provides a client API for Python to communicate with a running
[webdeck](./webdeck) instance.
